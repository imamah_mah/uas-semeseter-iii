from django.urls import path

from . import views 

urlpatterns = [
    path('', views.index, name='obat.index'),
    path('add', views.add, name='obat.add'),
    path('save', views.save, name='obat.save'),
    path('delete/<int:obat_id>/', views.delete, name='obat.delete'),
    path('edit/<int:obat_id>/', views.edit, name='obat.edit'),
    path('update/<int:obat_id>/', views.update, name='obat.update'),
]