from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from turtle import title
from django.shortcuts import render
from .models import Obat


def index(request):
    context = {
        'obat_list' : Obat.objects.all()
    }
    return render(request, 'pertama.html', context)

def add(request):
    return render(request, 'form.html')

def save(request):
    obat = Obat(
        nama_obat = request.POST['nama_obat'],
        jumlah_obat = request.POST['jumlah_obat'],
        kategori_obat = request.POST['kategori_obat'],
        tanggal = request.POST['tanggal'],
    )
    obat.save()

    return HttpResponseRedirect(reverse('obat.index'))

def delete(request, obat_id):
    obat = get_object_or_404(Obat, pk=obat_id)
    obat.delete()
    return HttpResponseRedirect(reverse('obat.index'))

def edit(request, obat_id):
    obat = get_object_or_404(Obat, pk=obat_id)
    tanggal = obat.tanggal.date()
    context = {
        'id' : obat_id,
        'nama_obat' : obat.nama_obat,
        'jumlah_obat' : obat.jumlah_obat,
        'kategori_obat' : obat.kategori_obat,
        'tanggal' : tanggal.strftime("%Y-%m-%d")
    }
    return render(request, 'form_edit.html', context)

def update(request, obat_id):
    obat = get_object_or_404(Obat, pk=obat_id)
    obat.nama_obat = request.POST['nama_obat']
    obat.jumlah_obat = request.POST['jumlah_obat']
    obat.kategori_obat = request.POST['kategori_obat']
    obat.tanggal = request.POST['tanggal']
    obat.save
   
    return HttpResponseRedirect(reverse('obat.index'))